(use-modules (guix packages)
             (guix download)
             (guix git-download)
             (gnu packages autotools)
             (gnu packages pkg-config)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages maths)
             (guix build-system gnu)
             ((guix licenses) #:prefix license:))

(define %source-dir (dirname (current-filename)))

(package
 (name "data-model")
 (version "0.1.0")
 (source #t)
 (build-system gnu-build-system)
 (native-inputs
  `(("pkg-config" ,pkg-config)
    ("autoconf" ,autoconf)
    ("automake" ,automake)))
 (inputs
  `(("guile" ,guile-3.0)
    ("guile-rdf", guile-rdf)))
 (synopsis "Experimentations into data model/data storage for openEngiadina")
 (description "Experimentations into data model/data storage for openEngiadina")
 (home-page "https://gitlab.com/openEngiadina/data-model")
 (license license:lgpl3+))
